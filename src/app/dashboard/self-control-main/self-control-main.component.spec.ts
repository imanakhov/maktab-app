import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfControlMainComponent } from './self-control-main.component';

describe('SelfControlMainComponent', () => {
  let component: SelfControlMainComponent;
  let fixture: ComponentFixture<SelfControlMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelfControlMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfControlMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
