import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { SelfControlMainComponent } from './self-control-main.component';

export const routes: Routes = [
  {
    path: 'self-control-main',
    component: SelfControlMainComponent,
    canActivate: []
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SelfControlMainRoutingModule {
}
