import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SelfControlMainRoutingModule } from './self-control-main-routing.module';

import { SelfControlMainComponent } from './self-control-main.component';

/**
 * Модуль Самоконроля
 * */
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SelfControlMainRoutingModule
  ],
  declarations: [
    SelfControlMainComponent
  ]
})
export class SelfControlMainModule {
}
