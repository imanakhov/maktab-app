import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { LessonMainRoutingModule } from './lesson-main-routing.module';

import { LessonMainComponent } from './lesson-main.component';

/**
 * Модуль курсов и предметов
 * */
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    LessonMainRoutingModule
  ],
  declarations: [
    LessonMainComponent
  ]
})
export class LessonMainModule { }
