import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { LessonMainComponent } from './lesson-main.component';

export const routes: Routes = [
  {
    path: 'lesson-main',
    component: LessonMainComponent,
    canActivate: []
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LessonMainRoutingModule {
}
