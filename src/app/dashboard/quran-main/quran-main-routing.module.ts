import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { QuranMainComponent } from './quran-main.component';

export const routes: Routes = [
  {
    path: 'quran-main',
    component: QuranMainComponent,
    canActivate: []
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuranMainRoutingModule {
}
