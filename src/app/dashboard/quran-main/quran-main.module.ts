import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { QuranMainRoutingModule } from './quran-main-routing.module';

import { QuranMainComponent } from './quran-main.component';

/**
 * Модуль изучения Корана
 * */
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    QuranMainRoutingModule
  ],
  declarations: [
    QuranMainComponent
  ]
})
export class QuranMainModule {
}
