import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { DashboardMainRoutingModule } from './dashboard-main-routing.module';

import { DashboardMainComponent } from './dashboard-main.component';

/**
 * Модуль Дашборд/личная страница
 * */
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DashboardMainRoutingModule
  ],
  declarations: [
    DashboardMainComponent
  ]
})
export class DashboardMainModule { }
