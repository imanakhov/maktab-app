import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { DashboardMainComponent } from './dashboard-main.component';

export const routes: Routes = [
  {
    path: 'dashboard-main',
    component: DashboardMainComponent,
    canActivate: []
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardMainRoutingModule {
}
