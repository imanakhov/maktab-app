import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { WebinarsComponent } from './webinars.component';

export const routes: Routes = [
  {
    path: '',
    component: WebinarsComponent,
    canActivate: []
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WebinarsRoutingModule {
}
