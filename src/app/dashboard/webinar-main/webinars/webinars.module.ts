import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { WebinarsRoutingModule } from './webinars-routing.module';

import { WebinarsComponent } from './webinars.component';
import { WebinarsArchiveComponent } from './webinars-archive/webinars-archive.component';
import { WebinarsComingModule } from './webinars-coming/webinars-coming.module';

/**
 * Модуль просмотра предстоящих и архивных вебинаров
 * */
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    WebinarsRoutingModule,
    WebinarsComingModule
  ],
  declarations: [
    WebinarsComponent,
    WebinarsArchiveComponent
  ]
})
export class WebinarsModule {
}
