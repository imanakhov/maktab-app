import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebinarsComingComponent } from './webinars-coming.component';

describe('WebinarsComingComponent', () => {
  let component: WebinarsComingComponent;
  let fixture: ComponentFixture<WebinarsComingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebinarsComingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebinarsComingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
