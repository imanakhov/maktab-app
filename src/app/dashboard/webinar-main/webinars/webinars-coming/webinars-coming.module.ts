import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { WebinarsComingRoutingModule } from './webinars-coming-routing.module';

import { WebinarsComingComponent } from './webinars-coming.component';
import { WebinarsCurrentComponent } from './webinars-current/webinars-current.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    WebinarsComingRoutingModule
  ],
  declarations: [
    WebinarsCurrentComponent,
    WebinarsComingComponent,
  ]
})
export class WebinarsComingModule {
}
