import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebinarsCurrentComponent } from './webinars-current.component';

describe('WebinarsCurrentComponent', () => {
  let component: WebinarsCurrentComponent;
  let fixture: ComponentFixture<WebinarsCurrentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebinarsCurrentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebinarsCurrentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
