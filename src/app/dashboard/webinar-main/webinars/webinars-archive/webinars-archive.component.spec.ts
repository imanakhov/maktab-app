import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebinarsArchiveComponent } from './webinars-archive.component';

describe('WebinarsArchiveComponent', () => {
  let component: WebinarsArchiveComponent;
  let fixture: ComponentFixture<WebinarsArchiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebinarsArchiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebinarsArchiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
