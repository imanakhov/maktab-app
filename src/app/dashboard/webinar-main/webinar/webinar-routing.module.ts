import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { WebinarComponent } from './webinar.component';

export const routes: Routes = [
  {
    path: '',
    component: WebinarComponent,
    canActivate: []
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WebinarRoutingModule {
}
