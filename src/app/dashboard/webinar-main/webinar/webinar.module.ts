import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { WebinarRoutingModule } from './webinar-routing.module';

import { WebinarComponent } from './webinar.component';

/**
 * Модуль просмтра одного вебинара
 * */
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    WebinarRoutingModule
  ],
  declarations: [
    WebinarComponent
  ]
})
export class WebinarModule {
}
