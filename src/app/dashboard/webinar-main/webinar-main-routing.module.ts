import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { WebinarMainComponent } from './webinar-main.component';
import { WebinarComponent } from './webinar/webinar.component';
import { WebinarsComponent } from './webinars/webinars.component';
import { WebinarsComingComponent } from './webinars/webinars-coming/webinars-coming.component';
import { WebinarsCurrentComponent } from './webinars/webinars-coming/webinars-current/webinars-current.component';
import { WebinarsArchiveComponent } from './webinars/webinars-archive/webinars-archive.component';

export const routes: Routes = [
  {
    path: 'webinar-main',
    component: WebinarMainComponent,
    canActivate: [],
    children: [
      {
        // Страница одного вебинара
        path: 'webinar',
        component: WebinarComponent
      },
      {
        // Роутинг вебинаров
        path: 'webinars',
        component: WebinarsComponent,
        children: [
          // Предстоящие (текущий || следующий месяцы)
          {
            path: 'coming',
            component: WebinarsComingComponent,
            children: [
              {
                path: 'current',
                component: WebinarsCurrentComponent
              }
            ]
          },
          // Архивные
          {
            path: 'archive',
            component: WebinarsArchiveComponent
          },
        ]
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WebinarMainRoutingModule {
}
