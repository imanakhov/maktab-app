import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { WebinarMainRoutingModule } from './webinar-main-routing.module';

import { WebinarMainComponent } from './webinar-main.component';
import { WebinarsModule } from './webinars/webinars.module';
import { WebinarModule } from './webinar/webinar.module';

/**
 * Модуль вебинаров
 * */
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    WebinarMainRoutingModule,
    WebinarsModule,
    WebinarModule,
  ],
  declarations: [
    WebinarMainComponent
  ]
})
export class WebinarMainModule {
}
