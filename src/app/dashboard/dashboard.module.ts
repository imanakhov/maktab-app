import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { DashboardRoutingModule } from './dashboard-routing.module';

import { DashboardComponent } from './dashboard.component';
import { DashboardMainModule } from './dashboard-main/dashboard-main.module';
import { LessonMainModule } from './lesson-main/lesson-main.module';
import { QuranMainModule } from './quran-main/quran-main.module';
import { SharedModule } from '../shared/shared.module';
import { WebinarMainModule } from './webinar-main/webinar-main.module';
import { SelfControlMainModule } from './self-control-main/self-control-main.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    DashboardRoutingModule,
    DashboardMainModule,
    LessonMainModule,
    QuranMainModule,
    WebinarMainModule,
    SelfControlMainModule
  ],
  declarations: [
    DashboardComponent
  ]
})
export class DashboardModule {
}
