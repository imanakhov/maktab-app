import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardMainComponent } from './dashboard/dashboard-main/dashboard-main.component';

const AppRoutes: Routes = [
  {
    path: '',
    canActivate: [],
    children: [
        // redirectTo: 'login'
      {
        path: '',
        redirectTo: 'dashboard-main',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        redirectTo: 'dashboard-main',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'dashboard-main',
        component: DashboardMainComponent
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forRoot(AppRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
