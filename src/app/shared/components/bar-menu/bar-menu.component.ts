import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bar-menu',
  templateUrl: './bar-menu.component.html',
  styleUrls: ['./bar-menu.component.scss']
})
export class BarMenuComponent implements OnInit {

  public selectedPopup = '';

  constructor() { }

  ngOnInit() {
  }

  openPopup(popupType: string) {
    this.selectedPopup = popupType;
  }

  closePopup() {
    this.selectedPopup = '';
  }

}
