import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { BarMenuComponent } from './components/bar-menu/bar-menu.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    NavMenuComponent,
    BarMenuComponent
  ],
  exports: [
    NavMenuComponent,
    BarMenuComponent
  ]
})
export class SharedModule { }
